-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-06-2018 a las 09:28:51
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `antumalen`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `disparos`
--

CREATE TABLE `disparos` (
  `DIS_id` int(10) UNSIGNED NOT NULL,
  `DIS_distancia` int(11) NOT NULL,
  `DIS_fecha_hora` datetime NOT NULL,
  `DI_estado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DI_id` int(10) UNSIGNED NOT NULL,
  `EX_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispositivos`
--

CREATE TABLE `dispositivos` (
  `DI_id` int(10) UNSIGNED NOT NULL,
  `DI_ventana_largo` int(11) NOT NULL,
  `DI_ventana_ancho` int(11) NOT NULL,
  `DI_historial` text COLLATE utf8mb4_unicode_ci,
  `ES_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `dispositivos`
--

INSERT INTO `dispositivos` (`DI_id`, `DI_ventana_largo`, `DI_ventana_ancho`, `DI_historial`, `ES_id`) VALUES
(1, 40, 50, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `espacios`
--

CREATE TABLE `espacios` (
  `ES_id` int(10) UNSIGNED NOT NULL,
  `ES_nombre` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ES_largo` int(11) NOT NULL,
  `ES_ancho` int(11) NOT NULL,
  `ES_alto` int(11) NOT NULL,
  `ES_num_ventanas` int(11) NOT NULL,
  `ES_num_personas` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `espacios`
--

INSERT INTO `espacios` (`ES_id`, `ES_nombre`, `ES_largo`, `ES_ancho`, `ES_alto`, `ES_num_ventanas`, `ES_num_personas`) VALUES
(1, 'CIMUBB', 12, 7, 3, 5, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `experimentos`
--

CREATE TABLE `experimentos` (
  `EX_id` int(10) UNSIGNED NOT NULL,
  `EX_nombre` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `EX_fecha_ini` date NOT NULL,
  `EX_hora_ini` time NOT NULL,
  `EX_fecha_fin` date NOT NULL,
  `EX_hora_fin` time NOT NULL,
  `EX_estado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `EX_di_aso` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ES_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_04_04_225801_create_espacios_table', 1),
(2, '2018_04_04_225802_create_dispositivos_table', 1),
(3, '2018_04_04_225842_create_experimentos_table', 1),
(4, '2018_04_04_225939_create_disparos_table', 1),
(5, '2018_04_10_223017_create_users_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', '$2y$10$41dRk4P9MBiMaqI6arENUerVdPTLPygNEZqU4hyDpld1aUuiU4yBe', 'W90RiamoQXfm7uz951YNb9rWHCLOle6acavjY2js5MDt80gC7ta1TA59H03f', NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `disparos`
--
ALTER TABLE `disparos`
  ADD PRIMARY KEY (`DIS_id`),
  ADD KEY `disparos_di_id_foreign` (`DI_id`),
  ADD KEY `disparos_ex_id_foreign` (`EX_id`);

--
-- Indices de la tabla `dispositivos`
--
ALTER TABLE `dispositivos`
  ADD PRIMARY KEY (`DI_id`),
  ADD KEY `dispositivos_es_id_foreign` (`ES_id`);

--
-- Indices de la tabla `espacios`
--
ALTER TABLE `espacios`
  ADD PRIMARY KEY (`ES_id`);

--
-- Indices de la tabla `experimentos`
--
ALTER TABLE `experimentos`
  ADD PRIMARY KEY (`EX_id`),
  ADD KEY `experimentos_es_id_foreign` (`ES_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `disparos`
--
ALTER TABLE `disparos`
  MODIFY `DIS_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dispositivos`
--
ALTER TABLE `dispositivos`
  MODIFY `DI_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `espacios`
--
ALTER TABLE `espacios`
  MODIFY `ES_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `experimentos`
--
ALTER TABLE `experimentos`
  MODIFY `EX_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `disparos`
--
ALTER TABLE `disparos`
  ADD CONSTRAINT `disparos_di_id_foreign` FOREIGN KEY (`DI_id`) REFERENCES `dispositivos` (`DI_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `disparos_ex_id_foreign` FOREIGN KEY (`EX_id`) REFERENCES `experimentos` (`EX_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `dispositivos`
--
ALTER TABLE `dispositivos`
  ADD CONSTRAINT `dispositivos_es_id_foreign` FOREIGN KEY (`ES_id`) REFERENCES `espacios` (`ES_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `experimentos`
--
ALTER TABLE `experimentos`
  ADD CONSTRAINT `experimentos_es_id_foreign` FOREIGN KEY (`ES_id`) REFERENCES `espacios` (`ES_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
