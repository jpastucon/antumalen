<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class DatabaseSeeder extends Seeder{
	
	public function run(){
		DB::table('users')->insert([
			'name' => 'admin',
			'email' => 'admin@admin.com',
			'password' => bcrypt('123456789'),
		]);
		DB::table('espacios')->insert([
			//'ES_id', 'ES_nombre', 'ES_largo', 'ES_ancho', 'ES_alto', 'ES_num_ventanas', 'ES_num_personas'
			'ES_id' => '88',
			'ES_nombre' => 'CIMUBB',
			'ES_largo' => '1200',
			'ES_ancho' =>	'700',
			'ES_alto' =>	'22',
			'ES_num_ventanas' => '5',
			'ES_num_personas' => '1',
		]);
		DB::table('dispositivos')->insert([
			//'DI_id', 'DI_ventana_largo', 'DI_ventana_ancho', 'DI_historial', 'ES_id'
			'DI_id' => '1',
			'DI_ventana_largo' => '40',
			'DI_ventana_ancho' => '30',
			'DI_historial' =>	' ',
			'ES_id' =>	'88',
		]);
	}
}

/*

INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('2', '12', '2018-06-15 03:01:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('3', '12', '2018-06-15 03:05:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('4', '12', '2018-06-15 03:10:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('5', '12', '2018-06-15 03:15:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('6', '12', '2018-06-15 03:20:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('7', '12', '2018-06-15 03:25:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('8', '12', '2018-06-15 03:30:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('9', '12', '2018-06-15 03:35:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('10', '12', '2018-06-15 03:40:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('11', '12', '2018-06-15 03:45:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('12', '12', '2018-06-15 03:50:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('13', '12', '2018-06-15 03:55:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('14', '12', '2018-06-15 :00:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('15', '12', '2018-06-15 04:05:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('16', '12', '2018-06-15 04:10:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('17', '12', '2018-06-15 04:05:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('18', '12', '2018-06-15 04:10:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('19', '12', '2018-06-15 04:15:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('20', '12', '2018-06-15 04:20:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('21', '12', '2018-06-15 04:25:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('22', '12', '2018-06-15 04:30:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('23', '12', '2018-06-15 04:35:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('24', '12', '2018-06-15 04:40:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('25', '12', '2018-06-15 04:45:00', 'Ventana Abierta', '1', '101');


INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('26', '12', '2018-06-16 03:01:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('27', '12', '2018-06-16 03:05:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('28', '12', '2018-06-16 03:10:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('29', '12', '2018-06-16 03:16:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('30', '12', '2018-06-16 03:20:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('31', '12', '2018-06-16 03:25:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('32', '12', '2018-06-16 03:30:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('33', '12', '2018-06-16 03:35:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('34', '12', '2018-06-16 03:40:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('35', '12', '2018-06-16 03:45:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('36', '12', '2018-06-16 03:50:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('37', '12', '2018-06-16 03:55:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('38', '12', '2018-06-16 :00:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('39', '12', '2018-06-16 04:05:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('40', '12', '2018-06-16 04:10:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('41', '12', '2018-06-16 04:05:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('42', '12', '2018-06-16 04:10:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('43', '12', '2018-06-16 04:16:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('44', '12', '2018-06-16 04:20:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('45', '12', '2018-06-16 04:25:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('46', '12', '2018-06-16 04:30:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('50', '12', '2018-06-16 04:35:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('51', '12', '2018-06-16 04:40:00', 'Ventana Abierta', '1', '101');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('52', '12', '2018-06-16 04:45:00', 'Ventana Abierta', '1', '101');

INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('53', '12', '2018-06-17 03:01:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('54', '12', '2018-06-17 03:05:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('55', '12', '2018-06-17 03:10:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('56', '12', '2018-06-17 03:17:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('57', '12', '2018-06-17 03:20:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('58', '12', '2018-06-17 03:25:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('59', '12', '2018-06-17 03:30:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('60', '12', '2018-06-17 03:35:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('61', '12', '2018-06-17 03:40:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('62', '12', '2018-06-17 03:45:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('63', '12', '2018-06-17 03:50:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('64', '12', '2018-06-17 03:55:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('65', '12', '2018-06-17 :00:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('66', '12', '2018-06-17 04:05:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('67', '12', '2018-06-17 04:10:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('68', '12', '2018-06-17 04:05:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('69', '12', '2018-06-17 04:10:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('70', '12', '2018-06-17 04:17:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('71', '12', '2018-06-17 04:20:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('72', '12', '2018-06-17 04:25:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('73', '12', '2018-06-17 04:30:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('74', '12', '2018-06-17 04:35:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('75', '12', '2018-06-17 04:40:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('76', '12', '2018-06-17 04:45:00', 'Ventana Abierta', '1', '102');

INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('77', '12', '2018-06-18 03:01:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('78', '12', '2018-06-18 03:05:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('79', '12', '2018-06-18 03:10:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('80', '12', '2018-06-18 03:18:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('81', '12', '2018-06-18 03:20:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('82', '12', '2018-06-18 03:25:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('83', '12', '2018-06-18 03:30:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('84', '12', '2018-06-18 03:35:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('85', '12', '2018-06-18 03:40:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('86', '12', '2018-06-18 03:45:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('87', '12', '2018-06-18 03:50:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('88', '12', '2018-06-18 03:55:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('89', '12', '2018-06-18 :00:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('90', '12', '2018-06-18 04:05:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('91', '12', '2018-06-18 04:10:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('92', '12', '2018-06-18 04:05:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('93', '12', '2018-06-18 04:10:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('94', '12', '2018-06-18 04:18:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('95', '12', '2018-06-18 04:20:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('96', '12', '2018-06-18 04:25:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('97', '12', '2018-06-18 04:30:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('98', '12', '2018-06-18 04:35:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('99', '12', '2018-06-18 04:40:00', 'Ventana Abierta', '1', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('100', '12', '2018-06-18 04:45:00', 'Ventana Abierta', '1', '102');


INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('101', '15', '2018-06-18 03:05:00', 'Ventana Abierta', '2', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('102', '15', '2018-06-18 03:10:00', 'Ventana Abierta', '2', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('103', '15', '2018-06-18 03:15:00', 'Ventana Abierta', '2', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('104', '15', '2018-06-18 03:20:00', 'Ventana Abierta', '2', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('105', '15', '2018-06-18 03:25:00', 'Ventana Abierta', '2', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('106', '15', '2018-06-18 03:30:00', 'Ventana Abierta', '2', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('107', '15', '2018-06-18 03:34:00', 'Ventana Abierta', '2', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('108', '15', '2018-06-18 03:43:00', 'Ventana Abierta', '2', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('109', '15', '2018-06-18 03:57:00', 'Ventana Abierta', '2', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('110', '15', '2018-06-18 04:05:00', 'Ventana Abierta', '2', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('111', '15', '2018-06-18 04:15:00', 'Ventana Abierta', '2', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('112', '15', '2018-06-18 04:25:00', 'Ventana Abierta', '2', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('113', '15', '2018-06-18 04:33:00', 'Ventana Abierta', '2', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('114', '15', '2018-06-18 04:39:00', 'Ventana Abierta', '2', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('115', '15', '2018-06-18 04:44:00', 'Ventana Abierta', '2', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('116', '15', '2018-06-18 05:50:00', 'Ventana Abierta', '2', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('117', '15', '2018-06-18 05:57:00', 'Ventana Abierta', '2', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('118', '15', '2018-06-18 05:58:00', 'Ventana Abierta', '2', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('119', '15', '2018-06-18 05:59:00', 'Ventana Abierta', '2', '102');
INSERT INTO `disparos` (`DIS_id`, `DIS_distancia`, `DIS_fecha_hora`, `DIS_estado`, `DI_id`, `EX_id`) VALUES ('120', '15', '2018-06-18 05:00:00', 'Ventana Abierta', '2', '102');
 */
///