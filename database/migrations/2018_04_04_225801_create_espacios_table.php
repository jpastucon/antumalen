<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspaciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espacios', function (Blueprint $table) {
            $table->increments('ES_id');
            $table->string('ES_nombre', 30);
            $table->integer('ES_largo');
            $table->integer('ES_ancho');
            $table->integer('ES_alto');
            $table->integer('ES_num_ventanas');
            $table->integer('ES_num_personas')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('espacios');
    }
}
