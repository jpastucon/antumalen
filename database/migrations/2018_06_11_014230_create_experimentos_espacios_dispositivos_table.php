<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperimentosEspaciosDispositivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experimentos_espacios_dispositivos', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status');
            $table->integer('EX_id')->unsigned();
            $table->foreign('EX_id')->references('EX_id')->on('experimentos')->onDelete('cascade')->onUpdate('cascade');;
            $table->integer('ES_id')->unsigned();
            $table->foreign('ES_id')->references('ES_id')->on('espacios')->onDelete('cascade')->onUpdate('cascade');;
            $table->integer('DI_id')->unsigned();
            $table->foreign('DI_id')->references('DI_id')->on('dispositivos')->onDelete('cascade')->onUpdate('cascade');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experimentos_espacios_dispositivos');
    }
}
