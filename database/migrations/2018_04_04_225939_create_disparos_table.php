<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisparosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disparos', function (Blueprint $table) {
            $table->increments('DIS_id');
            $table->integer('DIS_distancia');
            $table->dateTime('DIS_fecha_hora');
            $table->string('DIS_estado');
            //campos que contendran las llaves foraneas
            $table->unsignedInteger('DI_id');
            //establecemos las llaves foraneas, agregando la eliminacion y actualizacion en cascada
            $table->foreign('DI_id')->references('DI_id')->on('dispositivos')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disparos');
    }
}
