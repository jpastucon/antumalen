<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperimentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experimentos', function (Blueprint $table) {
            $table->increments('EX_id');
            $table->string('EX_nombre', 30);
             $table->date('EX_fecha_ini');
            $table->time('EX_hora_ini');
            $table->date('EX_fecha_fin');
            $table->time('EX_hora_fin');
            $table->string('EX_estado');
            $table->string('EX_di_aso');
            //campo que contendra la llave foranea
            $table->unsignedInteger('ES_id');
            //establecemos la llave foranea, agregando la eliminacion y actualizacion en cascada
            $table->foreign('ES_id')->references('ES_id')->on('espacios')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experimentos');
    }
}
