<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Experimentos;
use App\Dispositivos;

global $conexion;

class HomeController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $cont_exp_act = DB::table('experimentos')
        ->where('EX_estado', '=', 'Activo')
        ->count();
        $cont_exp_en_esp = DB::table('experimentos')
        ->where('EX_estado', '=', 'En espera')
        ->count();
        $cont_exp_fin = DB::table('experimentos')
        ->where('EX_estado', '=', 'Finalizado')
        ->count();



        $cont_dis = DB::table('dispositivos')->count();

        return view('home', compact(['cont_exp_act','cont_exp_en_esp','cont_exp_fin', 'cont_dis']));
    }


}
