<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dispositivos;
use App\Espacios;
use App\Disparos;
use Input;
use Session;
use Redirect;

class DispositivosController extends Controller{

    public function index(){
        $dispositivos = Dispositivos::all();
        return view('dispositivos.index',compact('dispositivos'));
    }

    public function create(){
        $espacios = Espacios::pluck('ES_nombre', 'ES_id');
        return view('dispositivos.create',compact('ES_id', 'espacios'));
    }

    public function store(Dispositivos $dispositivos){

        $dispositivos = new Dispositivos;
        $dispositivos->DI_id    = Input::get('DI_id');
        $dispositivos->DI_ventana_largo    = Input::get('DI_ventana_largo');
        $dispositivos->DI_ventana_ancho    = Input::get('DI_ventana_ancho');
        $dispositivos->ES_id    = Input::get('ES_id');
        $dispositivos->save();

        // redirect
        //Session::flash('message', 'Dispositivo creado ✔');
        Session::flash('alert-success', 'Dispositivo creado ✔');
        return Redirect::to('dispositivos');
    }

    public function show($id){
        $dispositivos = Dispositivos::where('DI_id', '=', $id)->get()->first();

        $disparos = Disparos::select('DIS_id', 'DIS_distancia', 'DIS_estado', 'DIS_fecha_hora', 'DI_id', 'EX_id')
        ->where('DI_id', '=', $id)
        ->get();

        return view('dispositivos.show', compact(['dispositivos', 'disparos']));
    }

    public function edit($id){
        $dispositivos = Dispositivos::where('DI_id', '=', $id)->get()->first();
        $espacios = Espacios::pluck('ES_nombre', 'ES_id');
        return view('dispositivos.edit', compact('dispositivos', 'espacios'));
    }

    public function update($id){
        //'DI_id', 'DI_ventana_largo', 'DI_ventana_ancho', 'DI_historial', 'ES_id'
        $dispositivos = Dispositivos::find($id);

        $dispositivos->DI_id    = Input::get('DI_id');
        $dispositivos->DI_ventana_largo    = Input::get('DI_ventana_largo');
        $dispositivos->DI_ventana_ancho    = Input::get('DI_ventana_ancho');
        $dispositivos->ES_id    = Input::get('ES_id');
        $dispositivos->save();

            // redirect
        //Session::flash('message', 'Dispositivo editado ✔');
        Session::flash('alert-info', 'Dispositivo actualizado ✔');
        return Redirect::to('dispositivos');

    }

    public function destroy($id){

        $dispositivos =Dispositivos::where('DI_id', '=', $id)->delete();

        Session::flash('alert-danger', 'Dispositivo eliminado ✔');

        return Redirect::to('dispositivos');
    }

}
