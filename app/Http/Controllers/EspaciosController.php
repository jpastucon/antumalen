<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Espacios;
use Input;
use Session;
use Redirect;

class EspaciosController extends Controller{

  public function index(){

    $espacios = Espacios::all();

    return view('espacios.index',compact('espacios'));
  }

  public function create(){
    return view('espacios.create');
  }

  public function store(Espacios $espacios){

   $espacios = new Espacios;
   $espacios->ES_nombre           = Input::get('ES_nombre');
   $espacios->ES_largo            = Input::get('ES_largo');
   $espacios->ES_ancho            = Input::get('ES_ancho');
   $espacios->ES_alto             = Input::get('ES_alto');
   $espacios->ES_num_ventanas     = Input::get('ES_num_ventanas');
   $espacios->ES_num_personas     = Input::get('ES_num_personas');
   $espacios->save();

        // redirect
   //Session::flash('message', 'Espacio creado ✔');
   Session::flash('alert-success', 'Espacio creado ✔');
   return Redirect::to('espacios');
 }

 public function show($id){
  $espacios = Espacios::where('ES_id', '=', $id)->get()->first();
  return view('espacios.show',compact('espacios'));
}

public function edit($id){
  $espacios = Espacios::where('ES_id', '=', $id)->get()->first();
  return view('espacios.edit',compact('espacios'));
}

public function update($id){

  $espacios = Espacios::find($id);
  $espacios->ES_nombre    = Input::get('ES_nombre');
  $espacios->ES_largo     = Input::get('ES_largo');
  $espacios->ES_ancho     = Input::get('ES_ancho');
  $espacios->ES_alto     = Input::get('ES_alto');
  $espacios->ES_num_ventanas     = Input::get('ES_num_ventanas');
  $espacios->ES_num_personas     = Input::get('ES_num_personas');
  $espacios->save();

            // redirect
  //Session::flash('message', 'Espacio actualizado ✔');
  
  Session::flash('alert-info', 'Espacio actualizado ✔');
  
  return Redirect::to('espacios');
}

public function destroy($id){

  $espacios =Espacios::where('ES_id', '=', $id)->delete();
  

  Session::flash('alert-danger', 'Espacio eliminado ✔');

  return Redirect::to('espacios');
}

}
