<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Disparos;

class DisparosController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(){
        // obtiene todo de disparos
		$disparos = Disparos::all();

        // load the view and pass the disparos
        return view('disparos.index',compact('disparos'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Disparos $disparos){
        // get the disparos
		 return view('disparos.show',compact('disparos'));
    }

    public function insert_disparo($distancia, $fecha, $hora, $estado, $DI_id){
        //estado = 0 : ABIERTO
        //estado = 1 : CERRADO
    
        $disparo = new Disparos;

        $disparo->DIS_distancia = $distancia;
        $disparo->DIS_fecha_hora = "$fecha $hora";
        if ($estado == 0) {
            $disparo->DIS_estado = "Abierto";    
        }elseif ($estado == 1) {
            $disparo->DIS_estado = "Cerrado";    
        }
                
        $disparo->DI_id = $DI_id;

        $disparo->save();
        
        echo "$distancia , $fecha $hora , $estado , $DI_id";

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function destroy(Disparos $disparos){
        $disparos->delete();

        return redirect()->route('disparos.index')
                        ->with('success','Disparo eliminado exitosamente');
    }


}
