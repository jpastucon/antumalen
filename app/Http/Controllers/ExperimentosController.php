<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Experimentos;
use App\Espacios;
use App\Dispositivos;
use App\Disparos;
use Input;
use Session;
use Redirect;
use Charts;


class ExperimentosController extends Controller{


    public function index(){
        $experimentos = Experimentos::all();
        $espacios = Espacios::pluck('ES_nombre', 'ES_id');
        $dispositivos = Dispositivos::all();
        return view('experimentos.index', compact(['experimentos', 'ES_id', 'espacios', 'dispositivos']));
    }


    public function create(){   
        return view('experimentos.create');
    }


    public function store(Experimentos $experimentos){
        //'EX_id', 'EX_nombre', 'EX_fecha_ini', 'EX_hora_ini', 'EX_fecha_fin', 'EX_hora_fin','EX_estado', 'EX_di_aso' , 'ES_id'

        $experimentos               = new Experimentos; 
        $experimentos->EX_nombre    = Input::get('EX_nombre');
        $experimentos->EX_fecha_ini = Input::get('EX_fecha_ini');
        $experimentos->EX_hora_ini  = Input::get('EX_hora_ini');
        $experimentos->EX_fecha_fin = Input::get('EX_fecha_fin');
        $experimentos->EX_hora_fin  = Input::get('EX_hora_fin');
        $experimentos->EX_estado    = '';
        $experimentos->ES_id        = Input::get('ES_id');

        $EX_di_aso    = Input::get('EX_di_aso');
        foreach ($EX_di_aso as &$valor) {
            $valor = $valor . ',';
        }

        $experimentos->EX_di_aso    = $valor;

        $fecha__actual = date('Y-m-d');
        
        if ($experimentos->EX_fecha_ini > $fecha__actual) {
             $experimentos->EX_estado = 'En espera';
        }
        if ($experimentos->EX_fecha_ini <= $fecha__actual && $experimentos->EX_fecha_fin > $fecha__actual) {
            $experimentos->EX_estado = 'Activo';
        }
        if ($experimentos->EX_fecha_fin < $fecha__actual) {
            $experimentos->EX_estado = 'Finalizado';
        }

        $experimentos->save();

        // redirect
        //Session::flash('message', 'Experimento creado ✔');
        Session::flash('alert-success', 'Experimento creado ✔');
        
        return Redirect::to('experimentos');

    }


    public function show($id){
        //'EX_id', 'EX_nombre', 'EX_fecha_ini', 'EX_hora_ini', 'EX_fecha_fin', 'EX_hora_fin','EX_estado', 'EX_di_aso' , 'ES_id'
        $experimentos = Experimentos::where('EX_id', '=', $id)->get()->first();


        //obtener los disparos, en relacion a uno o todos los dispositivos
        // 'DIS_id', 'DIS_distancia','DIS_estado', 'DIS_fecha_hora', 'DI_id', 'EX_id'
        
        $EX_fecha_ini = $experimentos->EX_fecha_ini;
        $EX_hora_ini = $experimentos->EX_hora_ini;
        $EX_fecha_hora_ini = "$EX_fecha_ini $EX_hora_ini";
        
        $EX_fecha_fin = $experimentos->EX_fecha_fin;
        $EX_hora_fin = $experimentos->EX_hora_fin;
        $EX_fecha_hora_fin = "$EX_fecha_fin $EX_hora_fin";

        $disparos = Disparos::select('DIS_id', 'DIS_distancia', 'DIS_estado', 'DIS_fecha_hora', 'DI_id')
        ->where('DIS_fecha_hora', '<=', $EX_fecha_hora_fin)
        ->where('DIS_fecha_hora', '>', $EX_fecha_hora_ini)
        ->get();

        $espacios = Espacios::where('ES_id', '=', $experimentos->ES_id)->get()->first();

        $total_dis_di = Disparos::select("SELECT count('DIS_id') as total, DI_id as DI_id
          FROM disparos INNER JOIN dispositivos 
          ON disparos.DI_id=dispositivos.DI_id GROUP BY DI_id");
        
        return view('experimentos.show', compact(['experimentos', 'disparos', 'espacios', 'total_dis_di']));
    }


    public function edit($id){

        $experimentos = Experimentos::where('EX_id', '=', $id)->get()->first();
        
        $espacios = Espacios::pluck('ES_nombre', 'ES_id');

        return view('experimentos.edit', compact(['experimentos', 'espacios']));

    }

    public function update($id){

        // 'EX_id', 'EX_nombre', 'EX_fecha_hora_ini', 'EX_fecha_hora_fin', 'ES_id'
        $experimento_edit = NULL; 
        $experimento_edit = Experimentos::find($id);

        $experimento_edit->EX_nombre    = Input::get('EX_nombre');
        $experimento_edit->EX_fecha_ini = Input::get('EX_fecha_ini');
        $experimento_edit->EX_hora_ini  = Input::get('EX_hora_ini');
        $experimento_edit->EX_fecha_fin = Input::get('EX_fecha_fin');
        $experimento_edit->EX_hora_fin  = Input::get('EX_hora_fin');
        $experimento_edit->EX_estado    = 'Activo';
        $experimento_edit->ES_id        = Input::get('ES_id');
        $experimento_edit->EX_di_aso    = Input::get('EX_di_aso');  
        
        $experimento_edit->save();

        //Session::flash('message', 'Experimento actualizado ✔');
        Session::flash('alert-info', 'Experimento actualizado ✔');

        return Redirect::to('experimentos');
    }

    public function destroy($id){

        $experimento =Experimentos::where('EX_id', '=', $id)->delete();

        //Session::flash('alert-warning', 'Experimento eliminado ✔');

        Session::flash('alert-danger', 'Experimento eliminado ✔');

        return Redirect::to('experimentos');
    }

}
