<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disparos extends Model{


	public $timestamps = false;

	
	protected $fillable = [
        'DIS_id', 'DIS_distancia','DIS_estado', 'DIS_fecha_hora', 'DI_id'
    ];

}
