<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dispositivos extends Model{


	public $timestamps = false;

	protected $primaryKey = 'DI_id';

	protected $fillable = [
        'DI_id', 'DI_ventana_largo', 'DI_ventana_ancho', 'DI_historial', 'ES_id'
    ];

    public function espacios(){
		return $this->belongsToMany('\App\Espacios','experimentos_espacios_dispositivos')
		->withPivot('ES_id','status');
	}

	public function experimentos(){
		return $this->belongsToMany('\App\Experimentos','experimentos_espacios_dispositivos')
		->withPivot('EX_id','status');
	}


}
