<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Espacios extends Model{

	public $timestamps = false;

	protected $primaryKey = 'ES_id';

	protected $fillable = [
        'ES_id', 'ES_nombre', 'ES_largo', 'ES_ancho', 'ES_alto', 'ES_num_ventanas', 'ES_num_personas'
    ];


    public function experimentos(){
		return $this->belongsToMany('\App\Experimentos','experimentos_espacios_dispositivos')
		->withPivot('EX_id','status');
	}
	
	public function dispositivos(){
		return $this->belongsToMany('\App\Dispositivos','experimentos_espacios_dispositivos')
		->withPivot('DI_id','status'); 
	}

}
