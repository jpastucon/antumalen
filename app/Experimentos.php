<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experimentos extends Model{

	public $timestamps = false;

	protected $primaryKey = 'EX_id';

	protected $fillable = [
		'EX_id', 'EX_nombre', 'EX_fecha_ini', 'EX_hora_ini', 'EX_fecha_fin', 'EX_hora_fin','EX_estado', 'EX_di_aso' , 'ES_id'
	];


	public function espacios(){
		return $this->belongsToMany('\App\Espacios','experimentos_espacios_dispositivos')
		->withPivot('ES_id','status');
	}

	public function dispositivos(){
		return $this->belongsToMany('\App\Dispositivos','experimentos_espacios_dispositivos')
		->withPivot('DI_id','status'); 
	}


}
