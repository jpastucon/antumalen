<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Sistema CIMUBB | Crear Dispositivo</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="/fonts/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="/fonts/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="/dist/css/AdminLTE.min.css">

	<link rel="stylesheet" href="/dist/css/skins/_all-skins.min.css">
	
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<!-- HEADER -->
		<header class="main-header">
			<!-- Logo -->
			<a href="/home" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>S</b>CIMUBB</span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>Sistema</b>CIMUBB</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top" role="navigation">
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="/dist/img/128.png" class="user-image" alt="User Image">
								<span class="hidden-xs">Administrador</span>
							</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header">
									<img src="/dist/img/128.png" class="img-circle" alt="User Image">
									<p>
										Administrador | Sistema CIMUBB
										<small>Member since March. 2018</small>
									</p>
								</li>
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<a href="#" class="btn btn-default btn-flat">Perfil</a>
									</div>
									<div class="pull-right">
										<a href="/logout" class="btn btn-default btn-flat">SALIR</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>


		<!-- MENU -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="/dist/img/128.png" class="img-circle" alt="User Image">
					</div>
					<div class="pull-left info">
						<p>Administrador</p>
						<a href="#"><i class="fa fa-circle text-success"></i>Online</a>
					</div>
				</div>
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li class="header">Panel de navegacion</li>
					<li><a href="/experimentos"><i class="ion ion-erlenmeyer-flask"></i> <span>Experimentos</span></a></li>
					<li><a href="/espacios"><i class="ion ion-map"></i> <span>Espacios</span></a></li>
					<li><a href="/dispositivos"><i class="ion-android-desktop"></i> <span>Dispositivos</span></a></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>


		<!-- Contenido -->
		<div class="content-wrapper">

			<!-- Breadcrumb -->
			<section class="content-header">
				<ol class="breadcrumb">
					<li><a href="/espacios"><i class="fa fa-dashboard"></i>Home</a></li>
					<li><a href="/dispositivos"><i class="fa fa-dashboard"></i>Dispositivo</a></li>
					<li><a href="/dispositivos/create"><i class="fa fa-dashboard"></i>Crear</a></li>
					<li class="active">Panel de Control</li>
				</ol>
			</section>
			<br>	

			<h1>Crear nuevo Dispositivo</h1>	
			<div class="container">
				<!-- if there are creation errors, they will show here -->
				{{ Html::ul($errors->all() )}}

				{{ Form::open(array('url' => 'dispositivos')) }}

				<div class="row">
					<div class="form-group"  style="padding-left: 30px; width:30%;">
						{{ Form::label('name', 'ID dispositivo') }}
						{{ Form::input('number', 'DI_id', null, ['id' => 'DI_id', 'class' => 'form-control', 'required' => 'required']) }}
						<br>
						{{ Form::label('name', 'Largo cm (VENTANA)') }}
						{{ Form::input('number', 'DI_ventana_largo', null, ['id' => 'DI_ventana_largo', 'class' => 'form-control', 'min' => 1, 'max' => 9999]) }}
						<br>			
						{{ Form::label('name', 'Ancho cm (VENTANA)') }}
						{{ Form::input('number', 'DI_ventana_ancho', null, ['id' => 'DI_ventana_ancho', 'class' => 'form-control', 'min' => 1, 'max' => 9999]) }}
						<br>
						{{ Form::label('name', 'Espacio Asociado') }}
						{{ Form::select('ES_id', $espacios, null, ['class' => 'form-control']) }}
					</div>
				</div>
				<div> 
					{{ Form::submit('GUARDAR', array('class' => 'btn btn-lg btn-primary')) }}

					{{ Form::close() }}

				</div>		
			</div> 
		</div>

	</div><!-- content-wrapper -->

	<!-- FOOTER -->
	<footer class="main-footer">
		<div class="pull-right hidden-xs">
			<b>Version</b> 2.3.0
		</div>
		<strong>Copyright &copy; 2018-2019 <a href="http://www.ubiobio.cl/cimubb/">Computer Integrated Manufacturing | University of Bio Bio</a>.</strong>Todos los derechos reservados.
	</footer>
</div> <!-- wrapper -->

<!-- jQuery 2.1.4 -->
<script src="/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="/bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="/plugins/moment.min.js"></script>
<script src="/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/dist/js/demo.js"></script>

</body>
</html>