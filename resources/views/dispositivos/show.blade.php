<!-- app/views/espacio/create.blade.php -->

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="/netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<title>Sistema CIMUBB | Mostrar Dispositivos</title>
	<meta content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no" name="viewport">

	<link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/fonts/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/fonts/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="/dist/css/AdminLTE.min.css">
	<link rel="stylesheet" type="text/css" href="/dist/css/skins/_all-skins.min.css">

</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<a href="/home" class="logo">
				<span class="logo-mini"><b>S</b>CIMUBB</span>
				<span class="logo-lg"><b>Sistema</b>CIMUBB</span>
			</a>
			<nav class="navbar navbar-static-top" role="navigarion">
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="/dist/img/128.png" class="user-image" alt="User Image">
								<span class="hidden-xs">Administrador</span>
							</a>
							<ul class="dropdown-menu">
								<li class="user-header">
									<img src="/dist/img/128.png" class="img-circle" alt="User Image">
									<p> Administrador | Sistema CIMUBB 
										<small>Member since March. 2018</small>			
									</p>							
								</li>
								<li class="user-footer">
									<div class="pull-left">
										<a href="#" class="btn btn-default btn-flat">Perfil</a>
									</div>
									<div class="pull-right">
										<a href="/logout" class="btn btn-default btn-flat">SALIR</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>

		<aside class="main-sidebar container-fluid">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="/dist/img/128.png" class="img-circle" alt="User Image">
					</div>
					<div class="pull-left info">
						<p>Administrador</p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li class="header">Panel de navegacion</li>
					<li><a href="/experimentos"><i class="ion ion-erlenmeyer-flask"></i> <span>Experimentos</span></a></li>
					<li><a href="/espacios"><i class="ion ion-map"></i> <span>Espacios</span></a></li>
					<li><a href="/dispositivos"><i class="ion-android-desktop"></i> <span>Dispositivos</span></a></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<div class="content-wrapper">
			<section class="content-header">
				<ol class="breadcrumb">
					<li><a href="/home"><i class="fa fa-dashboard"></i>Home</a></li>
					<li><a href="/experimentos"><i class="fa fa-dashboard"></i>Dispositivos</a></li>
					<li class="active">{{ $dispositivos->DI_id }}</li>
				</ol>
			</section>

			<div class="container" >
				<div class="container">
					
					<h1>Mostrando el dispositivo '{{ $dispositivos->DI_id }}'</h1>
					<div class="jumbotron text-left">						
						<strong>ID:</strong> {{ $dispositivos->DI_id }}<br><br>
						<strong>Espacio Asociado:</strong> {{ $dispositivos->ES_id }}<br><br>
						<strong>Ventana (LARGO):</strong> {{ $dispositivos->DI_ventana_largo }} cm<br>
						<strong>Ventana (ANCHO):</strong> {{ $dispositivos->DI_ventana_ancho }} cm<br><br>
						<strong>Historial:</strong> {{ $dispositivos->DI_historial }}<br><br>
					</div>
					<div>
						<h3>Listado de disparos para el dispositivo {{ $dispositivos->DI_id }}</h3>
						<table class="table table-striped table-bordered">
							<thead>
								<tr class="bg-aqua">
									<td>ID Disparo</td>
									<td>Distancia (cm)</td>
									<td>Fecha Hora Captura</td>
									<td>Estado de la ventana</td>
									<td>ID Dispositivo</td>
									<td>ID Experimento</td>
								</tr>
							</thead>
							<tbody>
								@foreach($disparos as $key => $value)
								<tr>
									<td>{{ $value->DIS_id }}</td>
									<td>{{ $value->DIS_distancia }} cm</td>
									<td>{{ $value->DIS_fecha_hora }}</td>
									<td>{{ $value->DIS_estado }}</td>
									<td>{{ $value->DI_id }}</td>
									<td>{{ $value->EX_id }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 2.3.0
			</div>
			<strong>Copyright &copy; 2018-2019 <a href="http://www.ubiobio.cl/cimubb/">Computer Integrated Manufacturing | University of Bio Bio</a>.</strong>Todos los derechos reservados.
		</footer>

		<div class="control-sidebar-bg"></div>


		<script src="/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="/plugins\jQueryUI/jquery-ui.min.js"></script>
		<!-- Bootstrap 3.3.5 -->
		<script src="/bootstrap/js/bootstrap.min.js"></script>
		<script> $.widget.bridge('uibutton', $.ui.button);</script>
		<script src="/plugins/raphael-min.js"></script>
		<script src="/plugins/morris/morris.min.js"></script>

		<script src="/plugins/morris/morris.min.js"></script>
		<!-- Sparkline -->
		<script src="/plugins/sparkline/jquery.sparkline.min.js"></script>
		<!-- jvectormap -->
		<script src="/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
		<script src="/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
		<!-- jQuery Knob Chart -->
		<script src="/plugins/knob/jquery.knob.js"></script>
		<!-- daterangepicker -->
		<script src="/plugins/moment.min.js"></script>
		<script src="/plugins/daterangepicker/daterangepicker.js"></script>
		<!-- datepicker -->
		<script src="/plugins/datepicker/bootstrap-datepicker.js"></script>
		<!-- Bootstrap WYSIHTML5 -->
		<script src="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
		<!-- Slimscroll -->
		<script src="/plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<!-- FastClick -->
		<script src="/plugins/fastclick/fastclick.min.js"></script>
		<!-- AdminLTE App -->
		<script src="/dist/js/app.min.js"></script>
		<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
		<script src="/dist/js/pages/dashboard.js"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="/dist/js/demo.js"></script>
	</body>
	</html>






