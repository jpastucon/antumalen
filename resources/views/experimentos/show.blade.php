<!-- app/views/espacio/create.blade.php -->

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="/netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<title>Sistema CIMUBB | Mostrar Experimento</title>
	<meta content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no" name="viewport">

	<link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/fonts/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/fonts/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="/dist/css/AdminLTE.min.css">
	<link rel="stylesheet" type="text/css" href="/dist/css/skins/_all-skins.min.css">

</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<a href="/home" class="logo">
				<span class="logo-mini"><b>S</b>CIMUBB</span>
				<span class="logo-lg"><b>Sistema</b>CIMUBB</span>
			</a>
			<nav class="navbar navbar-static-top" role="navigarion">
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="/dist/img/128.png" class="user-image" alt="User Image">
								<span class="hidden-xs">Administrador</span>
							</a>
							<ul class="dropdown-menu">
								<li class="user-header">
									<img src="/dist/img/128.png" class="img-circle" alt="User Image">
									<p> Administrador | Sistema CIMUBB 
										<small>Member since March. 2018</small>			
									</p>							
								</li>
								<li class="user-footer">
									<div class="pull-left">
										<a href="#" class="btn btn-default btn-flat">Perfil</a>
									</div>
									<div class="pull-right">
										<a href="/logout" class="btn btn-default btn-flat">SALIR</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>

		<aside class="main-sidebar container-fluid">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="/dist/img/128.png" class="img-circle" alt="User Image">
					</div>
					<div class="pull-left info">
						<p>Administrador</p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li class="header">Panel de navegacion</li>
					<li><a href="/experimentos"><i class="ion ion-erlenmeyer-flask"></i> <span>Experimentos</span></a></li>
					<li><a href="/espacios"><i class="ion ion-map"></i> <span>Espacios</span></a></li>
					<li><a href="/dispositivos"><i class="ion-android-desktop"></i> <span>Dispositivos</span></a></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<div class="content-wrapper">
			<section class="content-header">
				<ol class="breadcrumb">
					<li><a href="/home"><i class="fa fa-dashboard"></i>Home</a></li>
					<li><a href="/experimentos"><i class="fa fa-dashboard"></i>Experimentos</a></li>
					<li class="active">{{ $experimentos->EX_id }}</li>
				</ol>
			</section>

			<div class="container" >
				<div class="container">
					
					<center><h1>Mostrando el {{ $experimentos->EX_nombre }}</h1></center>
					<div >						
						<h3><strong>ID experimento:</strong> {{ $experimentos->EX_id }}<br></h3>
						<h3><strong>Nombre:</strong> {{ $experimentos->EX_nombre }}<br></h3>
						<h3><strong>Fecha Inicio:</strong> {{ $experimentos->EX_fecha_ini }}<br></h3>
						<h3><strong>Hora Inicio:</strong> {{ $experimentos->EX_hora_ini }}<br></h3>
						<h3><strong>Fecha Fin:</strong> {{ $experimentos->EX_fecha_fin }}<br></h3>
						<h3><strong>Hora Fin:</strong> {{ $experimentos->EX_hora_fin }}<br></h3>
						<h3><strong>Estado del experimento:</strong> {{ $experimentos->EX_estado }}<br></h3>
						<h3><strong>Espacio Asociado:</strong> {{ $espacios->ES_nombre }}<br></h3>
						<h3><strong>Dispositivos Asociados:</strong> {{ $experimentos->EX_di_aso }}</h3>
					</div>
					<br><br>
					<div>
						<strong><h3>Disparos realizados por los dispositivos</h3></strong>
						<table class="table table-striped table-bordered">
							<thead>
								<tr class="bg-aqua">
									<td><center>ID Disparo</center></td>
									<td><center>Distancia (cm)</center></td>
									<td><center>Fecha y Hora Captura</center></td>
									<td><center>Estado de la ventana</center></td>
									<td><center>ID Dispositivo</center></td>
								</tr>
							</thead>
							<tbody>
								@foreach($disparos as $key => $value)
								<tr>
									<td><center>{{ $value->DIS_id }}</center></td>
									<td><center>{{ $value->DIS_distancia }} cm</center></td>
									<td><center>{{ $value->DIS_fecha_hora }}</center></td>
									<td><center>{{ $value->DIS_estado }}</center></td>
									<td><center>{{ $value->DI_id }}</center></td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<br><br>
					<<div>

					</div>
				</div>
			</div>

		</div>

		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 2.3.0
			</div>
			<strong>Copyright &copy; 2018-2019 <a href="http://www.ubiobio.cl/cimubb/">Computer Integrated Manufacturing | University of Bio Bio</a>.</strong>Todos los derechos reservados.
		</footer>

		<div class="control-sidebar-bg"></div>
		<script src="/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="/plugins\jQueryUI/jquery-ui.min.js"></script>
		<!-- Bootstrap 3.3.5 -->
		<script src="/bootstrap/js/bootstrap.min.js"></script>
		<script> $.widget.bridge('uibutton', $.ui.button);</script>
		<script type="text/javascript" src="graficas/googlechart.js"></script>
		<script>
			var ctx = document.getElementById("grafica_total_dis_di").getContext('2d');
			var myChart = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
					datasets: [{
						label: '# of Votes',
						data: [12, 19, 3, 5, 2, 3],
						backgroundColor: [
						'rgba(255, 99, 132, 0.2)',
						'rgba(54, 162, 235, 0.2)',
						'rgba(255, 206, 86, 0.2)',
						'rgba(75, 192, 192, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(255, 159, 64, 0.2)'
						],
						borderColor: [
						'rgba(255,99,132,1)',
						'rgba(54, 162, 235, 1)',
						'rgba(255, 206, 86, 1)',
						'rgba(75, 192, 192, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(255, 159, 64, 1)'
						],
						borderWidth: 1
					}]
				},
				options: {
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero:true
							}
						}]
					}
				}
			});
			var myLineChart = new Chart(ctx, {
				type: 'line',
				data: data,
				options: options
			});
		</script>
		<script src="/plugins/raphael-min.js"></script>
		<script src="/plugins/morris/morris.min.js"></script>

		<script src="/plugins/morris/morris.min.js"></script>
		<!-- Sparkline -->
		<script src="/plugins/sparkline/jquery.sparkline.min.js"></script>
		<!-- jvectormap -->
		<script src="/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
		<script src="/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
		<!-- jQuery Knob Chart -->
		<script src="/plugins/knob/jquery.knob.js"></script>
		<!-- daterangepicker -->
		<script src="/plugins/moment.min.js"></script>
		<script src="/plugins/daterangepicker/daterangepicker.js"></script>
		<!-- datepicker -->
		<script src="/plugins/datepicker/bootstrap-datepicker.js"></script>
		<!-- Bootstrap WYSIHTML5 -->
		<script src="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
		<!-- Slimscroll -->
		<script src="/plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<!-- FastClick -->
		<script src="/plugins/fastclick/fastclick.min.js"></script>
		<!-- AdminLTE App -->
		<script src="/dist/js/app.min.js"></script>
		<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
		<script src="/dist/js/pages/dashboard.js"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="/dist/js/demo.js"></script>
	</body>
	</html>






