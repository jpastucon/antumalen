<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Sistema CIMUBB | Experimentos</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="fonts/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="fonts/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">

	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<!-- HEADER -->
		<header class="main-header" >
			<!-- Logo -->
			<a href="home" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>S</b>CIMUBB</span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>Sistema</b>CIMUBB</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top" role="navigation">
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="dist/img/128.png" class="user-image" alt="User Image">
								<span class="hidden-xs">Administrador</span>
							</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header">
									<img src="dist/img/128.png" class="img-circle" alt="User Image">
									<p>
										Administrador | Sistema CIMUBB
										<small>Member since March. 2018</small>
									</p>
								</li>
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<a href="#" class="btn btn-default btn-flat">Perfil</a>
									</div>
									<div class="pull-right">
										<a href="/logout" class="btn btn-default btn-flat">SALIR</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>

		<!-- MENU -->
		<aside class="main-sidebar container-fluid">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="dist/img/128.png" class="img-circle" alt="User Image">
					</div>
					<div class="pull-left info">
						<p>Administrador</p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li class="header">Panel de navegacion</li>
					<li><a href="/experimentos"><i class="ion ion-erlenmeyer-flask"></i> <span>Experimentos</span></a></li>
					<li><a href="/espacios"><i class="ion ion-map"></i> <span>Espacios</span></a></li>
					<li><a href="/dispositivos"><i class="ion-android-desktop"></i> <span>Dispositivos</span></a></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Contenido -->
		<div class="content-wrapper">

			<!-- Breadcrumb -->
			<section class="content-header">
				<ol class="breadcrumb">
					<li><a href="/home"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="/experimentos"><i class="fa fa-dashboard"></i> Experimentos</a></li>
					<li class="active">Panel de Control</li>
				</ol>
			</section>
			<br><br>

			<section class="content">
				<br><a class="btn btn-lg btn-primary btn-block" data-toggle="modal" data-target="#ModalConfExp">Configurar nuevo experimento</a>
			</br>
			<div class="flash-message">
				@foreach (['danger', 'warning', 'success', 'info'] as $msg)
				@if(Session::has('alert-' . $msg))
				<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				@endif
				@endforeach
			</div>
			<center><h1>Listado de Experimentos</h1></center>
			<table class="table table-striped table-bordered">
				<thead>
					<tr class="bg-aqua">
						<td>ID</td>
						<td>Nombre</td>
						<td>Fecha Hora Inicio</td>
						<td>Fecha Hora Fin</td>
						<td>Estado</td>
						<td>ID Espacio</td>
						<td>Acciones</td>
					</tr>
				</thead>
				<tbody>
					@foreach($experimentos as $key => $value)
					<tr>
						<td>{{ $value->EX_id }}</td>
						<td>{{ $value->EX_nombre }}</td>
						<td>{{ $value->EX_fecha_ini }} {{ $value->EX_hora_ini }}</td>
						<td>{{ $value->EX_fecha_fin }} {{ $value->EX_hora_fin }}</td>
						<td>{{ $value->EX_estado }}</td>
						<td>{{ $value->ES_id }}</td>
						<td>
							<a class="btn btn-small btn-success" href="{{ URL::to('experimentos/' . $value->EX_id) }}">📋 Detalles</a>

							<a class="btn btn-small btn-info" href="{{ URL::to('experimentos/' . $value->EX_id . '/edit') }}">📝 Editar</a>

							{{ Form::open(['method' => 'DELETE', 'route' => ['experimentos.destroy', $value->EX_id]])}}
							{{ Form::submit('❌ Eliminar', array('class' => 'btn btn-small btn-danger')) }}
							{{ Form::close() }}

						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

		</section>
	</div><!-- content-wrapper -->

	<div id="ModalConfExp" class="modal fade" role="dialog">
		<div class="modal-dialog">



			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<a class="btn btn-small btn-primary btn-block" href="{{ URL::to('espacios/create') }}" target="_blank">CREAR ESPACIO</a>
					<br><br>
					{{ Html::ul($errors->all() )}}

					{{ Form::open(array('url' => 'experimentos')) }}

					<div class="form-group">
						{{ Form::label('name', 'Nombre del experimento: ') }}
						{{ Form::input('text', 'EX_nombre', null, ['id' => 'EX_nombre', 'class' => '', 'required' => 'required']) }}
						<br><br>
						{{ Form::label('name', 'Seleccione un espacio para el experimento: ') }}
						{{ Form::select('ES_id', $espacios, null, ['id' => 'selectql', 'class' => 'form-inline']) }}
						<br><br>
						{{ Form::label('fixture_date', 'Fecha ') }}
						{{ Form::date('EX_fecha_ini', \Carbon\Carbon::now(), ['id' => 'EX_fecha_ini', 'class' => '', 'required' => 'required']) }}
						y 
						{{ Form::label('time', ' Hora ') }}
						{{ Form::input('time', 'EX_hora_ini', null, ['id' => 'EX_hora_ini', 'class' => '', 'required' => 'required']) }}
						de <b>INICIO</b>
						<br><br>
						{{ Form::label('fixture_date', 'Fecha ') }}
						{{ Form::date('EX_fecha_fin', \Carbon\Carbon::now()->addDay(), ['id' => 'EX_fecha_fin', 'class' => '', 'required' => 'required']) }}
						y 
						{{ Form::label('time', ' Hora ') }}
						{{ Form::input('time', 'EX_hora_fin', null, ['id' => 'EX_hora_fin', 'class' => '', 'required' => 'required']) }}
						de <b>TERMINO</b>
						<br><br>
						<div class="form-check" id="divDispositivos">
							{{ Form::label('name', 'Seleccione los dispositivos para experimento') }}
							<br><br>
							<?php 
							$all_data = array();
							foreach($dispositivos as $role){
								$all_data[] =  $role->DI_id;
							}
							?>
							@foreach($dispositivos as $key => $value)
							{{ Form::checkbox('EX_di_aso[]', $value->DI_id, in_array($value->DI_id, $all_data)) }}
							Dispositivo 
							{{ $value->DI_id }}
							<br>
							@endforeach
							<br><br>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					{{ Form::submit('Guardar', array('class' => 'btn btn-small btn-primary')) }} 
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
				{{ Form::close() }}	
			</div>
		</div>
	</div>


	<!-- FOOTER -->
	<footer class="main-footer">
		<div class="pull-right hidden-xs">
			<b>Version</b> 2.3.0
		</div>
		<strong>Copyright &copy; 2018-2019 <a href="http://www.ubiobio.cl/cimubb/">Computer Integrated Manufacturing | University of Bio Bio</a>.</strong>Todos los derechos reservados.
	</footer>
</div> <!-- wrapper -->

<!-- jQuery 2.1.4 -->
<script src="/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="/bootstrap/js/bootstrap.min.js"></script>
<script src="/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/dist/js/demo.js"></script>

<script type="text/javascript">

	$(document).ready(function(){
		//obtener el valor que esta en selectql (el seleccionado)
		//haces una consulta de los dispositivos del espacio
		//con jquery metes al div (divql) en este formato
		//	<input type="checkbox" class="form-check-input" id="DI_id">
		//<label class="form-check-label" for="DI_id">Dispositivo 1</label><br>
		$("#botonql").click(function(){
			alert("ESPACIO ID: " + $("select").attr("value"));
		});
	});
</script>

</body>
</html>