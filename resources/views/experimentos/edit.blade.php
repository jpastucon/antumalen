<!-- app/views/espacio/create.blade.php -->

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="/netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<title>Sistema CIMUBB | Mostrar Experimento</title>
	<meta content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no" name="viewport">

	<link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/fonts/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/fonts/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="/dist/css/AdminLTE.min.css">
	<link rel="stylesheet" type="text/css" href="/dist/css/skins/_all-skins.min.css">

</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<a href="/home" class="logo">
				<span class="logo-mini"><b>S</b>CIMUBB</span>
				<span class="logo-lg"><b>Sistema</b>CIMUBB</span>
			</a>
			<nav class="navbar navbar-static-top" role="navigarion">
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="/dist/img/128.png" class="user-image" alt="User Image">
								<span class="hidden-xs">Administrador</span>
							</a>
							<ul class="dropdown-menu">
								<li class="user-header">
									<img src="/dist/img/128.png" class="img-circle" alt="User Image">
									<p> Administrador | Sistema CIMUBB 
										<small>Member since March. 2018</small>			
									</p>							
								</li>
								<li class="user-footer">
									<div class="pull-left">
										<a href="#" class="btn btn-default btn-flat">Perfil</a>
									</div>
									<div class="pull-right">
										<a href="/logout" class="btn btn-default btn-flat">SALIR</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>

		<aside class="main-sidebar container-fluid">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="/dist/img/128.png" class="img-circle" alt="User Image">
					</div>
					<div class="pull-left info">
						<p>Administrador</p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li class="header">Panel de navegacion</li>
					<li><a href="/experimentos"><i class="ion ion-erlenmeyer-flask"></i> <span>Experimentos</span></a></li>
					<li><a href="/espacios"><i class="ion ion-map"></i> <span>Espacios</span></a></li>
					<li><a href="/dispositivos"><i class="ion-android-desktop"></i> <span>Dispositivos</span></a></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<div class="content-wrapper">
			<section class="content-header">
				<ol class="breadcrumb">
					<li><a href="/home"><i class="fa fa-dashboard"></i>Home</a></li>
					<li><a href="/experimentos"><i class="fa fa-dashboard"></i>Experimentos</a></li>
					<li><a href="/experimentos/edit"><i class="fa fa-dashboard"></i>Editar</a></li>
					<li class="active">{{ $experimentos->EX_id }}</li>
				</ol>
			</section>

			<div class="container" >

				{{ Form::model($experimentos, array('route' => array('experimentos.update', $experimentos->EX_id), 'method' => 'PUT')) }}


					<h3>Editando el experimento {{$experimentos->EX_id}}</h3>
					<div>

						<div class="form-horizontal">
							<div class="form-group" style="padding-left: 75px; padding-bottom: 10px; width:100%;">
								{!! Form::label('EX_nombre', 'Nombre Experimento:', ['class' => 'control-label col-sm-2']) !!}
								<div class="col-sm-2"> 
									{!! Form::text('EX_nombre', null, ['class' => 'form-control']) !!}
								</div>
							</div>
							<div class="form-group" style="padding-left: 75px; padding-bottom: 10px; width:100%;">
								{{ Form::label('fixture_date', 'Fecha ') }}
								{{ Form::date('EX_fecha_ini', $experimentos->EX_fecha_ini, ['id' => 'EX_fecha_ini', 'class' => '', 'required' => 'required']) }}
								y 
								{{ Form::label('time', ' Hora ') }}
								{{ Form::input('time', 'EX_hora_ini', $experimentos->EX_hora_ini, ['id' => 'EX_hora_ini', 'class' => '', 'required' => 'required']) }}
								de <b>INICIO</b>
							</div>
							<br>
							<div class="form-group" style="padding-left: 75px; padding-bottom: 10px; width:100%;">
								{{ Form::label('fixture_date', 'Fecha ') }}
								{{ Form::date('EX_fecha_fin', $experimentos->EX_fecha_fin, ['id' => 'EX_fecha_fin', 'class' => '', 'required' => 'required']) }}
								y 
								{{ Form::label('time', ' Hora ') }}
								{{ Form::input('time', 'EX_hora_fin', $experimentos->EX_hora_fin, ['id' => 'EX_hora_fin', 'class' => '', 'required' => 'required']) }}
								de <b>TERMINO</b>
							</div>
							<br>
							<div class="form-group" style="padding-left: 75px; padding-bottom: 10px; width:100%;">
								{!! Form::label('ES_id', 'Espacio asociado:', ['class' => 'control-label col-sm-2']) !!}
								<div class="col-sm-2"> 
									{{ Form::select('ES_id', $espacios, null, ['id' => 'selectql', 'class' => 'form-inline']) }}
								</div>
							</div>
							<div class="form-group" style="padding-left: 75px; padding-bottom: 10px; width:100%;">
								{!! Form::label('EX_dis_aso', 'IDs Dispositivos asociados:', ['class' => 'control-label col-sm-2']) !!}
								<div class="col-sm-2"> 
									{{ Form::text('EX_di_aso', null, ['class' => 'form-control']) }}
								</div>
							</div>
						</div>
					</div>
					<div class="form-group" style="padding-left: 75px; padding-bottom: 100px; width:100%;">
						{!! Form::submit('GUARDAR', ['class' => 'btn btn-primary col-sm-2']) !!}

						{!! Form::close() !!}
					</div>


				</div>

				<footer class="main-footer">
					<div class="pull-right hidden-xs">
						<b>Version</b> 2.3.0
					</div>
					<strong>Copyright &copy; 2018-2019 <a href="http://www.ubiobio.cl/cimubb/">Computer Integrated Manufacturing | University of Bio Bio</a>.</strong>Todos los derechos reservados.
				</footer>

				<div class="control-sidebar-bg"></div>


				<script src="/plugins/jQuery/jQuery-2.1.4.min.js"></script>
				<script src="/plugins\jQueryUI/jquery-ui.min.js"></script>
				<!-- Bootstrap 3.3.5 -->
				<script src="/bootstrap/js/bootstrap.min.js"></script>
				<script> $.widget.bridge('uibutton', $.ui.button);</script>
				<script src="/plugins/raphael-min.js"></script>
				<script src="/plugins/morris/morris.min.js"></script>

				<script src="/plugins/morris/morris.min.js"></script>
				<!-- Sparkline -->
				<script src="/plugins/sparkline/jquery.sparkline.min.js"></script>
				<!-- jvectormap -->
				<script src="/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
				<script src="/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
				<!-- jQuery Knob Chart -->
				<script src="/plugins/knob/jquery.knob.js"></script>
				<!-- daterangepicker -->
				<script src="/plugins/moment.min.js"></script>
				<script src="/plugins/daterangepicker/daterangepicker.js"></script>
				<!-- datepicker -->
				<script src="/plugins/datepicker/bootstrap-datepicker.js"></script>
				<!-- Bootstrap WYSIHTML5 -->
				<script src="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
				<!-- Slimscroll -->
				<script src="/plugins/slimScroll/jquery.slimscroll.min.js"></script>
				<!-- FastClick -->
				<script src="/plugins/fastclick/fastclick.min.js"></script>
				<!-- AdminLTE App -->
				<script src="/dist/js/app.min.js"></script>
				<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
				<script src="/dist/js/pages/dashboard.js"></script>
				<!-- AdminLTE for demo purposes -->
				<script src="/dist/js/demo.js"></script>
			</body>
			</html>






