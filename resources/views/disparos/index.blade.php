<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Sistema CIMUBB | Disparos</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="fonts/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="fonts/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">

	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<!-- HEADER -->
		<header class="main-header">
			<!-- Logo -->
			<a href="home" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>S</b>CIMUBB</span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>Sistema</b>CIMUBB</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top" role="navigation">
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="dist/img/128.png" class="user-image" alt="User Image">
								<span class="hidden-xs">Administrador</span>
							</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header">
									<img src="dist/img/128.png" class="img-circle" alt="User Image">
									<p>
										Administrador | Sistema CIMUBB
										<small>Member since March. 2018</small>
									</p>
								</li>
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<a href="#" class="btn btn-default btn-flat">Perfil</a>
									</div>
									<div class="pull-right">
										<a href="/logout" class="btn btn-default btn-flat">SALIR</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>

		<!-- MENU -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="dist/img/128.png" class="img-circle" alt="User Image">
					</div>
					<div class="pull-left info">
						<p>Administrador</p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li class="header">Panel de navegacion</li>
					<li><a href="experimentos"><i class="ion ion-erlenmeyer-flask"></i> <span>Experimentos</span></a></li>
					<li><a href="espacios/index"><i class="ion ion-map"></i> <span>Espacios</span></a></li>
					<li><a href="dispositivos"><i class="ion-android-desktop"></i> <span>Dispositivos</span></a></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Contenido -->
		<div class="content-wrapper ">

			<!-- Breadcrumb -->
			<section class="content-header">
				<h1>
					Disparos
				</h1>
				<ol class="breadcrumb">
					<li><a href="/home"><i class="fa fa-dashboard"></i>Home</a></li>
					<li><a href="disparos"><i class="fa fa-dashboard"></i> Disparos</a></li>
					<li class="active">Panel de control</li>
				</ol>
			</section>

			<section class="content">					
				<table class="table table-striped table-bordered">
					<thead>
						<tr class="bg-aqua">
							<th>ID</th>
							<th>Distancia</th>
							<th>Fecha - Hora</th>
							<th>ID dispositivo</th>
							<<th>Estado</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody>
					@foreach($disparos as $key => $value)
						<tr>
							<td>{{ $value->DIS_id }}</td>
							<td>{{ $value->DIS_distancia }}</td>
							<td>{{ $value->DIS_fecha_hora }}</td>
							<td>{{ $value->DI_id }}</td>
							<<td>{{ $value->DIS_estado }}</td>
							<td>

								<!-- delete the nerd (uses the destroy method DESTROY /disparos/{id} -->
								<!-- we will add this later since its a little more complicated than the first two buttons -->
								{{ Form::open(array('url' => 'disparos/' . $value->DIS_id, 'class' => 'pull-right')) }}
									{{ Form::hidden('_method', 'DELETE') }}
									{{ Form::submit('¿Eliminar este disparo?', array('class' => 'btn btn-warning')) }}
								{{ Form::close() }}

								<!-- show the nerd (uses the show method found at GET /disparos/{DIS_id} -->
								<a class="btn btn-small btn-success" href="{{ URL::to('disparos/' . $value->DIS_id) }}">Mostrar</a>

							</td>
						</tr>
					@endforeach
				</tbody>
				</table>
			</section>


		</div>



		</div><!-- content-wrapper -->

		<!-- FOOTER -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 2.3.0
			</div>
			<strong>Copyright &copy; 2018-2019 <a href="http://www.ubiobio.cl/cimubb/">Computer Integrated Manufacturing | University of Bio Bio</a>.</strong>Todos los derechos reservados.
		</footer>
	</div> <!-- wrapper -->

	<!-- jQuery 2.1.4 -->
	<script src="/plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="/bootstrap/js/bootstrap.min.js"></script>
	<script src="/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="/dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="/dist/js/demo.js"></script>

</body>
</html>