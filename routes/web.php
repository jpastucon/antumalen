<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/insert_disparos/{distancia}/{fecha}/{hora}/{estado}/{DI_id}', 'DisparosController@insert_disparo');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::resource('disparos', 'DisparosController');

Route::resource('espacios', 'EspaciosController');

Route::resource('dispositivos', 'DispositivosController');

Route::resource('experimentos', 'ExperimentosController');

Route::post('experimentos/{EX_id}/edit', 'ExperimentosController@edit');

Route::resource('ventanas', 'VentanasController');

Route::get('/home', 'HomeController@index')->name('home');
